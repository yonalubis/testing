<?php

class DbHandler {
 
    private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . './DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function upload()
    {
        $uploaddir = 'images/';
        $uploadfile = $_FILES['userfile']['name'];
        $response = array();
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploaddir.$uploadfile)) 
        {
            $response["result"] = "1";
            echo json_encode($response);
        } else 
        {
            $response["result"] = "-1";
            echo json_encode($response);
        }
    }

    public function SaveVideo($video, $name) {
        $path = "images/" . $name . ".mp4";
        file_put_contents($path,base64_decode($video));
        $ret = "SUCCESS";
        return $ret;
    }

    public function createLokasi($video) {
        $ret = array();
        $video = "images/" . $video;
        $query = "INSERT INTO lokasi (lokasi.video) VALUES ('$video')";
        if ($this->conn->query($query) === TRUE) {
            $result = "SUCCESS";
        } else {
            $result = "";
        }
        return $result;
    }

    public function getLokasi() {
        $ret = array();
        $index = 0;
        $query = "SELECT lokasi.id_lokasi, lokasi.nama, lokasi.telp, lokasi.lat, lokasi.lang, lokasi.address, lokasi.statename, lokasi.countryname, lokasi.images FROM lokasi";
        $result = $this->conn->query($query);
        while ($task = $result->fetch_assoc()) {
            $tmp = array();
            $tmp["id_lokasi"] = $task["id_lokasi"];
            $tmp["nama"] = $task["nama"];
            $tmp["telp"] = $task["telp"];
            $tmp["lat"] = $task["lat"];
            $tmp["lang"] = $task["lang"];
            $tmp["address"] = $task["address"];
            $tmp["statename"] = $task["statename"];
            $tmp["countryname"] = $task["countryname"];
            $tmp["images"] = $task["images"];
            array_push($ret, $tmp);
        }
        return $ret;
    }

	/**
	* Verifying required params posted or not
	*/
	function verifyRequiredParams($required_fields) {
		$error = false;
		$error_fields = "";
		$request_params = array();
		$request_params = $_REQUEST;
		
		// Handling PUT request params
		if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$app = \Slim\Slim::getInstance();
			parse_str($app->request()->getBody(), $request_params);
		}
		foreach ($required_fields as $field) {
			if  (!isset($request_params[$field])  || 
				strlen(trim($request_params[$field])) <= 0) {
				$error = true;
				$error_fields .= $field . ', ';
			}
		}
		if ($error) {
			// Required field(s) are missing or empty
			// echo error json and stop the app
			$response = array(); 
			$app = \Slim\Slim::getInstance();
			$response["error"] = true;
			$response["message"]  =  'Required  field(s)  '  . 
			substr($error_fields, 0, -2) . ' is missing or empty';
			echoRespnse(400, $response);
			$app->stop();
		}
	}
 
}
 
?>